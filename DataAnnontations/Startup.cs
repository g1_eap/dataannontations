﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DataAnnontations.Startup))]
namespace DataAnnontations
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
